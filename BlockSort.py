import os
import sys
import copy

class BlockSort:
    def __init__(self):
        self.FILE_RAW = "File_Raw.txt"
        self.FILE_SORTED = "File_Sorted.txt"
        self.block=[] # contain pair of TermID - DocID
        self.invert_block = [] # contain termID - posting list
        self.block_n = 0
        self.cur_docID = 0
        self.term_dict = {}
        self.list_file = os.listdir("docs/")

        self.BLOCK_SIZE_LIMIT = 64*1024  # 64 KB
        self.cur_block_size = 0

    def parse(self, term, docID):
        if term in self.term_dict:
            termID = self.term_dict[term] # access termID
        else:
            termID = len(self.term_dict)
            self.term_dict[term] = termID # add termID

        termID_docID = (termID, docID)
        if termID_docID not in self.block:
            self.block.append(termID_docID)
            self.cur_block_size += sys.getsizeof(termID) + sys.getsizeof(docID)


    def refresh_block(self):
        """ Refresh block in memory"""
        self.block=[] # contain pair of TermID - DocID
        self.invert_block = [] # contain termID - posting list
        self.cur_block_size = 0 # set cur_block_size to 0
        self.block_n += 1 # increment block counting


    def invert(self):
        self.block.sort(key=lambda x: (x[0], x[1]))
        cur_termID = -1;
        cur_posting_list =[]
        for entry in self.block:
            tmp_docid = entry[1]
            tmp_termid = entry[0]
            if cur_termID is -1:
                cur_termID = entry[0]
            if cur_termID is not tmp_termid:
                new_posting_list = copy.deepcopy(cur_posting_list)
                invert_item = (cur_termID,new_posting_list)
                self.invert_block.append(invert_item)
                cur_termID = tmp_termid
                cur_posting_list = []
            cur_posting_list.append(tmp_docid)


    def write_to_disk(self,file_name, array_of_content, cur_block_id):
        f = open(file_name, "a")
        tag = "block_"+str(cur_block_id)+"\n"
        f.write(tag)
        for e in array_of_content:
            if file_name is self.FILE_RAW:
                tmp = str(e[0]) + " " + str(e[1]) + "\n"
                f.write(tmp)

            elif file_name is self.FILE_SORTED:
                tmp = str(e[0]) + " "
                f.write(tmp)
                for docid in e[1]:
                    f.write("--")
                    f.write(str(docid))
                f.write("\n")

    def BSBIndexConstruction(self):
        for i in range(0, len(self.list_file)):  # for each file
            #  print (file_name)
            file_name = self.list_file[i]
            with open("nostopword/"+file_name, "r") as r:  # open it
                for line in r: # read line by line
                    for w in line: # read by word
                        self.parse(term=w,docID=i)
                        if self.cur_block_size >= self.BLOCK_SIZE_LIMIT:
                            self.write_to_disk(self.FILE_RAW, self.block, self.block_n)  # write tup (termID - docID)
                            self.invert()
                            self.write_to_disk(self.FILE_SORTED,self.invert_block,self.block_n)  # write posting list
                            self.refresh_block()
        # do something to the remained in block
        self.write_to_disk(self.FILE_RAW, self.block, self.block_n)
        self.invert()
        self.write_to_disk(self.FILE_SORTED, self.invert_block, self.block_n)
        self.refresh_block()