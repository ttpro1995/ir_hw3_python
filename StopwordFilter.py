import re
import string

class StopwordFilter:
    def __init__(self):
        self.DOCS = "docs/"
        self.NO_STOP_WORD ="nostopword/"
        self.stop_word_list = []
        self.word_in_file = ""
        self.file_name =""

    def read_stopword(self):
        stopword = open("stopwords_en.txt").read().split();
        self.stop_word_list = stopword;

    def addWord(self, word):
        if word not in self.stop_word_list:
            self.word_in_file+=word+" "

    def process_raw_file(self, file_name):
        self.word_in_file = ""

        self.file_name = file_name

        file_content = open(self.DOCS+file_name).read().splitlines()
        for s in file_content:
            out = s.translate(str.maketrans("", "", string.punctuation)) # remove punctuation from word
            self.addWord(out)

    def write_to_file(self):
        file = open(self.NO_STOP_WORD+self.file_name, "w")
        file.write(self.word_in_file)
        file.close()