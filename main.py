import os
import time

from StopwordFilter import *
from BlockSort import *

def main():
    sw_filter = StopwordFilter()
    list_file = os.listdir("docs/")

    start = time.time()
    for file in list_file:
        sw_filter.process_raw_file(file)
        sw_filter.write_to_file()
        #print(file)
    end1 = time.time()
    print("stop word remove in ", end1 - start)

    # Block sort
    blocksort = BlockSort()
    blocksort.BSBIndexConstruction()
    end2 = time.time()
    print("blocksort in ", end2 - end1)

if __name__=="__main__":
    main()